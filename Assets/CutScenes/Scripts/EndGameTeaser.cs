﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndGameTeaser : MonoBehaviour
{
    public Camera cam;
    public GameObject boss;

    private Rigidbody2D camRb2d;
    private GameObject hole;
    private GameObject tunnel;
    private Player player;
    private Image whiteScreen; //inside UICanvas

    private string[][] playerMessages = new string[][] {
        new string[]{ "What's happening?" }
    };
    private string[][] bossMessages = new string[][] {
        new string[]{ "This place has no used for me anymore.", "I will rebuild it." }
    };

    void Awake()
    {
        camRb2d = cam.GetComponent<Rigidbody2D>();
        hole = GameObject.Find("Hole");
        tunnel = GameObject.Find("Tunnel");
        tunnel.SetActive(false);
        player = GameObject.Find("Player").GetComponent<Player>();
        whiteScreen = GameObject.Find("WhiteScreen").GetComponent<Image>();
    }

    void Start()
    {
        // Enter cut scene ('disable' Player.Update())
        GameSettings.instance.cutScene = true;
        // Disable camera follow
        cam.GetComponent<CameraFollow>().enabled = false;
        // Stop camera movement
        cam.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        StartCoroutine(RunCutScene());
    }

    IEnumerator RunCutScene()
    {
        // Wait for everything to settle down
        yield return new WaitForSeconds(0.25f);

        // Ground shake, player starts asking why
        yield return StartCoroutine(GroundShake(-1f, 0f, 1f, 0.02f));
        GameManager.instance.sendTextBoxMessage(playerMessages[0]);

        // JLTS moves in & has some words
        yield return StartCoroutine(BossMoveIn(1.5f, 0.02f, Vector3.left * 0.066f));
        GameManager.instance.sendTextBoxMessage(bossMessages[0]);

        // JLTS flies away
        StartCoroutine(GroundShake(-1f, 0f, 2f, 0.02f));
        yield return StartCoroutine(BossMoveIn(2f, 0.02f, Vector3.up * 0.05f));

        // Ground shake even move
        StartCoroutine(GroundShake(-2f, 5f, 100f, 0.02f));

        // A tunnel appears after about 2 seconds
        yield return new WaitForSeconds(2f);
        hole.SetActive(false);
        tunnel.SetActive(true);

        // Player automatically jumps down the hole and escapes
        yield return new WaitForSeconds(0.75f);
        GameSettings.instance.cutSceneRunning = 1;

        // Screen white out
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(WhiteOutScreen());

        // Wait for few seconds then switch to credit scene
        yield return new WaitForSeconds(5f);
        Application.LoadLevel("Credits");
        SoundManager.instance.PlayMusic("credits");
    }

    IEnumerator WhiteOutScreen()
    {
        float alpha = 0.015625f;
        for (int i = 0; i < 64; ++i, alpha += 0.015625f)
        {
            whiteScreen.color = new Color(1f, 1f, 1f, alpha);
            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator BossMoveIn(float duration, float interval, Vector3 lerpVect)
    {
        while (duration > 0)
        {
            boss.transform.localPosition += lerpVect;
            duration -= interval;
            Debug.Log("bossMove duration left = " + duration);
            yield return new WaitForSeconds(interval);
        }
    }

    IEnumerator GroundShake(float camShakeY, float addedCamShakeX, float duration, float interval)
    {
        Vector3 camOriginalPos = cam.transform.localPosition;
        // Shake camera
        while (duration > 0)
        {
            float camShakeX = Random.RandomRange(-3f, 3f);
            camShakeX = camShakeX + Mathf.Sign(camShakeX) * Mathf.Abs(addedCamShakeX);
            camRb2d.velocity = new Vector2(camShakeX, camShakeY);
            if (cam.transform.localPosition.y > 0.5f) //cam shakes too high
                camShakeY = -Mathf.Abs(camShakeY);
            else
                camShakeY = -camShakeY;
            duration -= interval;
            yield return new WaitForSeconds(interval);
        }
        // Reset camera after shaking
        camRb2d.velocity = Vector2.zero;
        cam.transform.localPosition = camOriginalPos;
    }
}
