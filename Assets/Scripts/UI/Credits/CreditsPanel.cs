﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CreditsPanel : MonoBehaviour
{
    public Text title;
    public Text[] lines;

    private Image image;
    private CanvasGroup canvasGroup;

    private List<Credits> credits = new List<Credits>();
    private int cid = 0;

    void Awake()
    {
        image = GetComponent<Image>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void Start()
    {
        credits.Add(new Credits("Team Lead", "James Wang"));
        credits.Add(new Credits("Lead Programmer", "Samuel Huang"));
        credits.Add(new Credits("Programmers", "Samuel Huang", "Tri Tran"));
        credits.Add(new Credits("Lead Artist", "Luis Barrios"));
        credits.Add(new Credits("Animations", "Luis Barrios", "Tri Tran"));
        credits.Add(new Credits("Level Designer", "James Wang", "Samuel Huang"));
        credits.Add(new Credits("Music", "Mark Sparling", "(marksparling.com)"));
        credits.Add(new Credits("Sound Effects", "Volterock & undocument",
            "Blake Hastings", "Brandon Shaw", "Chris Williams"));
        credits.Add(new Credits("Sound Effects", "Volterock & undocument",
            "Dave Ventura", "Squishy Flip", "Von Volterock"));
        credits.Add(new Credits("Sound Effects", "Tobiasz 'unfa' Karon", "Tri Tran"));
        credits.Add(new Credits("Fonts", "\"bladeline\" by designstation", "\"Ace Futurism\" by NAL"));
        NextCredit();
    }

    public void NextCredit()
    {
        Credits crd = credits[cid];
        title.text = crd.title;
        int i = 0;
        for (; i < crd.names.Length; ++i)
            lines[i].text = crd.names[i];
        for (; i < lines.Length; ++i)
            lines[i].text = "";
        cid = (cid + 1) % credits.Count;
    }

    void FixedUpdate()
    {
        canvasGroup.alpha = image.color.a;
    }
}
