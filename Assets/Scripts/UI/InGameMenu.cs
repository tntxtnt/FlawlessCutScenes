﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InGameMenu : MonoBehaviour
{
    public GameObject pausePanel;
    public GameObject optionPanel;
    public GameObject confirmationPanel;

    private Canvas ui;
    private bool Active;
    private bool StopedTime;

    void Awake()
    {
        ui = GetComponent<Canvas>();
    }

    void Start()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(false);
        ui.enabled = false;
    }

    void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            if (Active)
                Unpause();
            else
                Pause();
        }
    }

    void OnEnable()
    {
        ToPauseMenu();
    }

    void Pause()
    {
        StopedTime = Time.timeScale != 0;
        Time.timeScale = 0;
        Active = true;
        ui.enabled = true;
        ToPauseMenu();
        GameManager.instance.Pause();
    }

    void Unpause()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(false);
        ui.enabled = false;
        Active = false;
        if (StopedTime)
        {
            Time.timeScale = 1;
        }
        GameManager.instance.Unpause();
    }

    void ToMainMenu()
    {
        Unpause();
        GameManager.instance.reset();
        GameManager.instance.Load("MainMenu", "main");
    }

    void ToPauseMenu()
    {
        pausePanel.SetActive(true);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(false);
    }

    void ToOptionMenu()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(true);
        confirmationPanel.SetActive(false);
    }
}
