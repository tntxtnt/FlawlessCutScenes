﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SmoothScroll : MonoBehaviour
{
    public float scrollSpeed = 0.2f;
    public float destination = -1f;
    
    private ScrollRect scroll;

    void Awake()
    {
        scroll = GetComponent<ScrollRect>();
    }

    void Update()
    {
        if (Mathf.Abs(Input.GetAxis("Mouse X")) > 0f || Mathf.Abs(Input.GetAxis("Mouse Y")) > 0f
            || Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            if (destination > 0f)
            {
                scroll.verticalNormalizedPosition = destination;
                destination = -1f;
            }
        }
    }

    void LateUpdate()
    {
        if (scroll == null || destination < 0f) return;
        float delta = destination - scroll.verticalNormalizedPosition;
        if (Mathf.Abs(delta) < Mathf.Epsilon)
        {
            destination = -1f;
            return;
        }
        scroll.verticalNormalizedPosition += delta * scrollSpeed;
    }
}
