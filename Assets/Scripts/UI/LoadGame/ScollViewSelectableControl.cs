﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScollViewSelectableControl : MonoBehaviour, IPointerEnterHandler, ISelectHandler, IDeselectHandler, IPointerExitHandler
{
    public Color baseSelectedColor = Color.white;
    public int selectSound = 1;
    public ScrollRect scroll;
    public SmoothScroll smoothScroll;

    private Selectable selectable;
    private Color normalButtonImageColor;
    private KeyboardUIControl kbControl;

    private float height;
    private float k;

    private static int minIndex = 0;

    void Awake()
    {
        selectable = GetComponent<Selectable>();
        normalButtonImageColor = selectable.image.color;
        kbControl = GameObject.Find("MenuCanvas").GetComponent<KeyboardUIControl>();
    }

    void Start()
    {
        height = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
        k = 1f / (transform.parent.childCount - 3.35f);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        selectable.image.color = baseSelectedColor;
        kbControl.Deselect();
        SoundManager.instance.PlaySingle(SoundManager.instance.buttonSelect1);
    }

    public void OnSelect(BaseEventData eventData)
    {
        selectable.image.color = baseSelectedColor;
        if (kbControl.hasSelected)
        {
            SoundManager.instance.PlaySingle(SoundManager.instance.buttonSelect1);

            int i = int.Parse(transform.name.Split(' ')[1]);
            float newPos = scroll.verticalNormalizedPosition;
            float normalizedPos = k * i;
            float pos = 1f - 5f / height - normalizedPos;
            if (i > minIndex + 2)
            {
                newPos = Mathf.Max(0f, pos + 2.5f * k);
                ++minIndex;
            }
            else if (i < minIndex)
            {
                newPos = Mathf.Max(0f, pos);
                --minIndex;
            }

            smoothScroll.destination = newPos;
        }
    }

    public void OnDeselect(BaseEventData eventData)
    {
        selectable.image.color = normalButtonImageColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        selectable.image.color = normalButtonImageColor;
    }
}
