﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConfirmLoad : MonoBehaviour
{
    public GameObject gameInfoPanel;

    private string savePath = "./Saves/";
    private string saveType = ".jlts";

    void Start()
    {
        if (System.IO.File.Exists(savePath + GameManager.instance.fileName + saveType))
        {
            string[] lines = System.IO.File.ReadAllLines(savePath + GameManager.instance.fileName + saveType);
            GameObject savedGame = Instantiate(gameInfoPanel);
            savedGame.transform.SetParent(GameObject.Find("MenuCanvas").GetComponent<RectTransform>());
            savedGame.transform.localScale = Vector3.one;
            savedGame.transform.localPosition = new Vector3(0f, 75f, 0f);
            savedGame.transform.FindChild("GameName").gameObject.GetComponent<Text>().text = GameManager.instance.fileName;
            savedGame.transform.FindChild("UILifeBar").GetComponent<UILifeBar>().open(int.Parse(lines[6]), int.Parse(lines[5]));
            savedGame.transform.FindChild("LastPlayed").GetComponent<Text>().text = "Last played: " + lines[15];
        }
    }
}
