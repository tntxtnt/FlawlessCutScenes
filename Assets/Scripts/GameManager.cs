﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;/*
using System.Collections;
using System.Text;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;*/

public class GameManager : MonoBehaviour
{
/*   [Serializable]
    public struct Data
    {
        public string Name;
    }
    */
    private Text KeyCount;
    public string fileName;
	public string continueFileName;

    public bool first;
    public float newXpos = 0;
    public float newYpos = 0;
    public float newCamXpos = 0;
    public float newCamYpos = 0;
    public int PlayerFlip = 1;
    public int GlobalAttackUp = 0;
    public int PlayerHp = 10;
    public int PlayerMaxHp = 10;
    public int Keys = 0;
    public bool DoubleJump = false;
    public bool Attack2 = false;
    public bool Attack3 = false;
    public bool Bow = false;
    public bool BossKey = false;
    public bool[] GottenItem;
    public System.Random random = new System.Random();

    public static GameManager instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            first = true;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        loadGameSetting();
    }
    public void Load(string name, string songName)
    {
        if(!name.Equals("MainMenu"))
            first = false;
        Application.LoadLevel(name);
        SoundManager.instance.PlayMusic(songName);
	}
	public void newGameLoad()
	{
		Application.LoadLevel("Level0");
		SoundManager.instance.PlayMusic("sky");
	}
    public void addAttack(int attackUp)
    {
        GlobalAttackUp+=attackUp;
        GameObject.Find("Player").GetComponent<Player>().attackup();
    }
    public void addHp(int HpUp)
    {
        PlayerMaxHp += HpUp;
        if (PlayerMaxHp > 100)
            PlayerMaxHp = 100;
        GameObject.Find("Player").GetComponent<Player>().MaxHpup();
        GameObject.Find("Player").GetComponent<Player>().Heal(HpUp);
    }
    public void updateKeys(int amount)
    {
        Keys += amount;
        if (Keys < 0)
            Keys = 0;
        if (Keys > 99)
            Keys = 99;
        KeyCount = GameObject.Find("Number").GetComponent<Text>();
        KeyCount.text = getKeyString();
    }
    public void reset()
    {
        newXpos = 0;
        newYpos = 0;
        PlayerFlip = 1;
        GlobalAttackUp = 0;
        PlayerHp = 10;
        PlayerMaxHp = 10;
        Keys = 0;
		DoubleJump = false;
        BossKey = false;
        Attack2 = false;
        Attack3 = false;
        Bow = false;
        int size = GottenItem.Length;
        for (int i = 0; i < size; i++)
            GottenItem[i]= false;
        first = true;
    }
	public void newSave()
	{
		string[] text = new string[16];
		text[0] = "new";
		text[5] = "10";
		text[6] = "10";
		text [14] = fileName;
        DateTime localDate = DateTime.Now; 
        text[15] = localDate.ToString(new CultureInfo("en-US"));
        System.IO.File.WriteAllLines("./Saves/"+fileName+ ".jlts", text);
		continueFileName = fileName;
		saveGameSetting();
	}
    public void Save(string LevelName,string SongName)
    {
		string[] text = new string[20];
        text[0] = LevelName;
        text[1] = ""+GameObject.Find("Player").transform.position.x;
        text[2] = ""+GameObject.Find("Player").transform.position.y;
        text[3] = ""+GameObject.Find("Player").GetComponent<Player>().getFlip(); ;
        text[4] = "" + GlobalAttackUp;
        text[5] = "" + GameObject.Find("Player").GetComponent<Player>().getHP();
        text[6] = "" + PlayerMaxHp;
        int size = GottenItem.Length;
        text[7] = "" + size;
        text[8] = "";
        for (int i=0;i<size;i++)
        {
            if(GottenItem[i])
                text[8] += "1";
            else
                text[8] += "0";
        }
        text[9] = SongName;
        text[10] = "" + GameObject.Find("Main Camera").transform.position.x;
        text[11] = "" + GameObject.Find("Main Camera").transform.position.y;
		text[12] = "" + Keys;
		if(DoubleJump)
			text[13] += "1";
		else
			text[13] += "0";
		text [14] = fileName;
        DateTime localDate = DateTime.Now;
        text[15] = localDate.ToString(new CultureInfo("en-US"));
        if (BossKey)
            text[16] += "1";
        else
            text[16] += "0";
        if (Attack2)
            text[17] += "1";
        else
            text[17] += "0";
        if (Attack3)
            text[18] += "1";
        else
            text[18] += "0";
        if (Bow)
            text[19] += "1";
        else
            text[19] += "0";
    System.IO.File.WriteAllLines("./Saves/"+fileName+ ".jlts", text);
		continueFileName = fileName;
        saveGameSetting();
	}
	public void LoadSave()
	{
		if (System.IO.File.Exists("./Saves/" + fileName + ".jlts"))
		{
			string[] lines = System.IO.File.ReadAllLines("./Saves/" + fileName + ".jlts");
			string name = lines[0];
			if(name.Equals("new"))
			{
				first = true;
				newGameLoad();
				return;
			}
			newXpos = float.Parse(lines[1]);
			newYpos = float.Parse(lines[2]);
			PlayerFlip = int.Parse(lines[3]);
			GlobalAttackUp = int.Parse(lines[4]);
			PlayerHp = int.Parse(lines[5]);
			PlayerMaxHp = int.Parse(lines[6]);
			int size = int.Parse(lines[7]);
			GottenItem = new bool[size];
			for (int i = 0; i < size; i++)
			{
				if (lines[8][i] == '1')
					GottenItem[i] = true;
				else
					GottenItem[i] = false;
			}
			string song = lines[9];
			newCamXpos = float.Parse(lines[10]);
			newCamYpos = float.Parse(lines[11]);
			Keys = int.Parse(lines[12]);
			DoubleJump = lines[13].Equals("1");
			continueFileName = lines[14];
            BossKey = lines[16].Equals("1");

            Attack2 = lines[17].Equals("1");
            Attack3 = lines[18].Equals("1");
            Bow = lines[19].Equals("1");

            saveGameSetting();
			Load(name, song);
			first = false;
		}
	}
	public void saveGameSetting()
	{
		string[] text = new string[12];
		text[0] = continueFileName;
		if(GameSettings.instance.muteSfx)
			text[1] += "1";
		else
			text[1] += "0";
		if(GameSettings.instance.muteMusic)
			text[2] += "1";
		else
			text[2] += "0";
		text [3] = "" + GameSettings.instance.musicVolume;
		text[4] = ""+GameSettings.instance.sfxVolume;
        System.IO.File.WriteAllLines("./GameSettings.jlts", text);
    }
    private void loadGameSetting()
    {
		if (System.IO.File.Exists ("./GameSettings.jlts")) 
		{
			string[] lines = System.IO.File.ReadAllLines ("./GameSettings.jlts");
			continueFileName = lines[0];
			bool muteSfx = lines[1].Equals("1");
			bool muteMusic = lines[2].Equals("1");
			float musicVolume = float.Parse(lines[3]);
			float sfxVolume = float.Parse(lines[4]);

			GameSettings.instance.ChangeMusicVolume (musicVolume);
			GameSettings.instance.ToggleMusic (muteMusic);
			GameSettings.instance.ChangeSfxVolume (sfxVolume);
			GameSettings.instance.ToggleSfx (muteSfx);
		}
    }
	public void LoadContinue()
	{
		fileName = continueFileName;
		LoadSave ();
	}
    private string getKeyString()
    {
        string returnString = "x ";
        returnString += Keys;
        return returnString;
    }
    public void Pause()
    {
        GameObject.Find("TextBox").GetComponent<TextBox>().Pause();
    }
    public void Unpause()
    {
        GameObject.Find("TextBox").GetComponent<TextBox>().Unpause();
        GameObject.Find("Player").GetComponent<Player>().Unpause();
    }
    public void sendTextBoxMessage(string [] message)
    {
        GameObject.Find("TextBox").GetComponent<TextBox>().SetMessage(message);
    }
    public void sendTextBoxMessage(string[] message,int [] fontSize)
    {
        GameObject.Find("TextBox").GetComponent<TextBox>().SetMessage(message, fontSize);
    }
    public void getBossKey()
    {
        BossKey = true;
        ShowBossKey();
    }
    public void useBossKey()
    {
        BossKey = false;
        ShowBossKey();
    }
    public void ShowBossKey()
    {
        GameObject.Find("BossKeyUI").GetComponent<SpriteRenderer>().enabled = BossKey;
    }

    /*
    public static void SerializeItem(string fileName)
    {
        // Create an instance of the type and serialize it.
        IFormatter formatter = new BinaryFormatter();
        Data t = new Data();
        t.Name = "Hello World";

        FileStream s = new FileStream(fileName, FileMode.Create);
        formatter.Serialize(s, t);
        s.Close();
    }


    public static void DeserializeItem(string fileName)
    {
        IFormatter formatter = new BinaryFormatter();
        FileStream s = new FileStream(fileName, FileMode.Open);
        Data t = (Data)formatter.Deserialize(s);
        print(t.Name);
    }*/
}
