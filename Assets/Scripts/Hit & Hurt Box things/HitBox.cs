﻿using UnityEngine;
using System.Collections;

public class HitBox : MonoBehaviour
{

	// Use this for initialization
	public HitAble parent;
	private Collider2D boxCollider;
	public int damage;
    public int hitStun;
    public float knockbackX;
    public float knockbackY;
    void Awake () 
	{
		boxCollider = GetComponent<Collider2D>();
	}
	
	public void setParent(HitAble actor)
	{
		parent = actor;
	}
	public void setDamage(int value)
	{
		damage = value;
	}
	public int getDamage()
	{
		return damage;
	}

	public void setEnable(bool value)
	{
		boxCollider.enabled = value;
    }
    void OnCollisionEnter2D(Collision2D hit)
    {
        if (parent != null)
            parent.HitConnected(hit);
    }
}
