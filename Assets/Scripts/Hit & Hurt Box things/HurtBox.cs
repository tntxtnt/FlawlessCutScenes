﻿using UnityEngine;
using System.Collections;

public class HurtBox : MonoBehaviour
{
    
	private  HitAble parent;
	public void setParent(HitAble actor)
	{
		parent = actor;
	}
	public HitAble getParent()
	{
		return parent;
	}
	void OnCollisionEnter2D(Collision2D hit)
	{
        if(parent != null)
		    parent.gotHit (hit);
	}
    public void off()
    {
        transform.GetComponent<BoxCollider2D>().enabled = false;
    }
}
