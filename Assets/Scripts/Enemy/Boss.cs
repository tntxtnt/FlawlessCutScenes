﻿using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour, HitAble
{
    //Boss Right X = -7    Y = -12
    //Boss Left  X = -17.2 Y = -12

    private Rigidbody2D body;
    private Animator animator;
    private SpriteRenderer spriterenderer;
    private Transform player;
    private Moveable moveable;
    private HitBox hitBox2;
    private HurtBox hurtBox;
    private BoxCollider2D box;
    private Vector2 Left = new Vector2(-17.2f,-12);
    private Vector2 Right = new Vector2(-7,-12);
    private Vector2 Mid = new Vector2(-12.1f,-12);    

    private bool attaking;
    private int Hitstun;
    private int invulnerable;
    private int flip;
    private int flipCooldown;
    private int bombCooldown;
    private bool throwFlip=true;
    private int Pos = 1;
    private GameObject [] Blocks;
    private bool madeBlocks;
    private bool patternOver;

    public bool bomb;
    public float moveSpeed;
    public float jumpSpeed;
    public int Hp;
    public int invincibilityFrames;
    public GameObject BOMB;
    public GameObject ARROW;
    public GameObject ARROWDOWN;
    public GameObject BLOCK;
    public float bombXOffset;
    public float bombYOffset;
    public float bombStartX;
    public float bombStartY;
    public int cooldown;
    public int ArrowHold;


    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        box = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
        spriterenderer = GetComponent<SpriteRenderer>();
        player = GameObject.Find("Player").transform;
        moveable = new Moveable(body);

        hurtBox = transform.GetChild(0).GetComponent<HurtBox>();
        hitBox2 = transform.GetChild(1).GetComponent<HitBox>();

        hurtBox.setParent(this);
        hitBox2.setParent(this);

        flipCooldown = 0;
        if (transform.localScale.x > 0)
            flip = -1;
        else
            flip = 1;
        animator.SetBool("Run", true);
    }

    void FixedUpdate()
    {
        if (Hitstun != 0)
        {
            runHitstun();
        }
        else if (attaking)
        {
            runAttack();
        }
        else
        {
            checkAttack();
        }
        runInvulnerable();
        moveable.CheckY();
    }

    public void gotHit(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            if (invulnerable == 0)
            {
                animator.SetBool("Hit", true);
                runDamageCalcuation(hit);
                hitBox2.setEnable(false);
                AttackRecovery();
                AttackOver();
            }
        }
    }
    public void HitConnected(Collision2D hit) { }

    private void runDamageCalcuation(Collision2D hit)
    {
        HitBox gotHitBox = hit.gameObject.GetComponent<HitBox>();
        Hp -= gotHitBox.getDamage();
        if (Hp < 0)
            Hp = 0;
        int direction = 1;
        if (hit.gameObject.transform.position.x - transform.position.x < 0)
            direction = -1;
        moveable.moveY(gotHitBox.knockbackY);
        moveable.setForceX(gotHitBox.knockbackX * direction);
        Hitstun = gotHitBox.hitStun;
        if (Hp == 0)
        {
            Hitstun = 150;
            animator.SetBool("Dead", true);
            hurtBox.off();
        }
        animator.SetBool("Hitstun", true);
    }

    private bool Grounded()
    {
        float HalfWidth = ((transform.lossyScale.x * box.size.x) / 2.0f);
        return GroundRaycastCheck(0) || GroundRaycastCheck(HalfWidth) || GroundRaycastCheck(-HalfWidth);
    }
    private bool GroundRaycastCheck(float offset)
    {
        float HalfHeight = (transform.lossyScale.y * box.size.y) / 2.0f;
        RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(transform.position.x + offset, transform.position.y - HalfHeight + .1f), Vector2.down, .15f);
        int size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("Ground"))
            {
                return true;
            }
        }
        return false;
    }


    private float getDistance()
    {
        return Mathf.Sqrt(Mathf.Pow(player.position.x - transform.position.x, 2) + Mathf.Pow((player.position.y - transform.position.y), 2));
    }
    private bool wallRaycastCheck()
    {
        RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(transform.position.x, transform.position.y), Vector2.left, .1f);
        int size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("EnemyWall"))
            {
                return true;
            }
        }
        hit = Physics2D.RaycastAll(new Vector2(transform.position.x, transform.position.y + .28f), Vector2.right, .1f);
        size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("EnemyWall"))
            {
                return true;
            }
        }
        return false;
    }
    private void checkAttack()
    {
        if (bombCooldown == 0)
        {
            animator.SetBool("Attack", true);
            attaking = true;
        }
        else
            bombCooldown--;
    }
    private void runHitstun()
    {
        animator.SetBool("Hit", false);
        if (Grounded())
            moveable.moveX(0);
        else
            moveable.ForceX();
        if (Hitstun == 1)
        {
            if (Hp <= 0)
                DestroyObject(this.gameObject);
            else
            {
                animator.SetBool("Hitstun", false);
                invulnerable = invincibilityFrames;
            }
            hitBox2.setEnable(true);
            flipCooldown = 10;
        }
        Hitstun--;
    }
    private void runAttack()
    {
        moveable.moveX(0);
    }
    private void runInvulnerable()
    {

        if (invulnerable != 0)
        {
            invulnerable--;
            if (invulnerable % 3 == 1)
                spriterenderer.enabled = false;
            else
                spriterenderer.enabled = true;
        }
    }

    public void AttackActive()
    {
        if (bomb)
        {
            for (int i = 0; i < 8; i++)
            {
                if (throwFlip && i % 2 == 0)
                    makeBomb((.8f * i));
                else if (!throwFlip && i % 2 == 1)
                    makeBomb((.8f * i));
            }
            throwFlip = !throwFlip;
        }
        else
        {
            if (patternOver)
            {
                destroyBlock();
                patternOver = false;
            }
            else if (madeBlocks)
            {
                if (throwFlip)
                {
                    if (Pos != 0)
                    {
                        for (int i = -2; i < 30; i++)
                        {
                            makeArrow((.2f * i));
                        }
                    }
                    else
                    {
                        for (int i = -27; i < 28; i++)
                        {
                            makeArrowDown((.2f * i));
                        }
                    }
                }
                else
                {
                    if (Pos == 1)
                        MoveLeft();
                    else if (Pos == -1)
                        MoveMid();
                    else if (Pos == 0)
                    {
                        patternOver = true;
                        MoveRight();
                    }
                }
                throwFlip = !throwFlip;
            }
            else
                makeBlock();
        }
    }
    private void makeBomb(float Xoff)
    {
        GameObject bomb = Instantiate(BOMB, new Vector3(transform.position.x + (bombXOffset * flip), transform.position.y + bombYOffset, transform.position.z), Quaternion.identity) as GameObject;
        Bomb TempBomb = bomb.GetComponent<Bomb>();
        TempBomb.start();
        TempBomb.move((bombStartX - Xoff) * flip, bombStartY);
    }
    private void makeArrow(float Yoff)
    {
        GameObject arrow = Instantiate(ARROW, new Vector3(transform.position.x + (bombXOffset * flip), transform.position.y + Yoff, transform.position.z), Quaternion.identity) as GameObject;
        Arrow TempArrow = arrow.GetComponent<Arrow>();
        TempArrow.start();
        TempArrow.Flip(flip);
        TempArrow.setHold(ArrowHold);
    }
    private void makeArrowDown(float Xoff)
    {
        GameObject arrow = Instantiate(ARROWDOWN, new Vector3(transform.position.x + (Xoff * flip), transform.position.y+5.5f, transform.position.z), ARROWDOWN.transform.localRotation) as GameObject;
        Arrow TempArrow = arrow.GetComponent<Arrow>();
        TempArrow.start();
        TempArrow.Flip(flip);
        TempArrow.setHold(ArrowHold);
    }
    private void makeBlock()
    {
        float[] pattern = blockPattern1();
        int length = pattern.Length;
        Blocks = new GameObject[length / 2];
        for (int i = 0; i < length; i += 2)
        {
            Blocks[i / 2] = Instantiate(BLOCK, new Vector3(pattern[i], pattern[i + 1], transform.position.z), Quaternion.identity) as GameObject;
        }
        madeBlocks = true;
    }
    public void destroyBlock()
    {
        int length = Blocks.Length;
        for (int i = 0; i < length; i++)
        {
            GameObject.Destroy(Blocks[i].gameObject);
        }
        madeBlocks = false;
    }
    private void MoveRight()
    {
        transform.position = Right;
        Pos = 1;
        if(flip!=-1)
            moveable.flip(transform);
        flip = -1;
    }
    private void MoveLeft()
    {
        transform.position = Left;
        Pos = -1;
        if(flip!=1)
            moveable.flip(transform);
        flip = 1;
    }
    private void MoveMid()
    {
        transform.position = Mid;
        Pos = 0;
    }
    public void AttackRecovery()
    {
    }
    public void AttackOver()
    {
        attaking = false;
        animator.SetBool("Attack", false);
        flipCooldown = 10;
        bombCooldown = cooldown;
    }
    private float[] blockPattern1()
    {
        float[] pattern =
            {
                -13.76f   , -9.35f,
                -14.84f   , -11f,
                -10.18f   , -10.86f,
                -15.11823f, -7.895771f,
                -12.5f    , -10.41f,
                -11.25f   , -9.94f
            };
        return pattern;
    }
}
