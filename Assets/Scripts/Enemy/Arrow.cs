﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour, HitAble
{
    public string hitTag;
    public float speed;
    public bool Down;

    private int Hold = 0;
    private Moveable moveable;
    private HitBox box;
    private int flip = 1;

    // Use this for initialization
    void Start ()
    {
        start();
	}
	
	// Update is called once per frame
	void Update () {}
    void FixedUpdate()
    {
        if (Hold == 0)
        {
            if (!Down)
                moveable.moveX(speed * flip);
            else
                moveable.moveY(-speed);
        }
        else
            Hold--;
    }
    void OnCollisionEnter2D(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Untagged"))
            DestroyObject(this.gameObject);
    }
    public void gotHit(Collision2D hit){}
    public void HitConnected(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals(hitTag))
            DestroyObject(this.gameObject);
    }
    public void start()
    {
        moveable = new Moveable(GetComponent<Rigidbody2D>(),0);
        box = transform.GetChild(0).GetComponent<HitBox>();
        box.setParent(this);
    }
    public void Flip(int Flip)
    {
        flip = Flip;
        if (flip == -1)
            moveable.flip(transform);
    }
    public void setSpeed(float Speed)
    {
        speed = Speed;
    }
    public void setDamage(int value)
    {
        box.setDamage(value);
    }
    public void setHold(int amount)
    {
        Hold = amount;
    }
}
