﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour, HitAble
{
    private Collider2D childBox;
    private BoxCollider2D box;
    private ParticleSystem fire;
    private SpriteRenderer bomb;
    private Rigidbody2D body;
    private Moveable moveable;
    private HurtBox hurtBox;
    private bool active;
    private bool fixGround;

    public int Timer;
    public int ActiveTime;
    public float maxDownVelocity;
    public bool airExplode;
    public float factor;
    void Start ()
    {
        start();   
	}
	
	void Update ()
    {
        fixGround = Grounded();
    }
    void FixedUpdate()
    {
        if (fixGround)
            moveable.moveX(0);
        else
            moveable.ForceX();
        if (!active)
        {
            if (Timer <= 0 && (fixGround||airExplode))
            {
                fire.Play(); ;
                childBox.enabled = true;
                bomb.enabled = false;
                active = true;
            }
            Timer--;
        }
        else
        {
            if(ActiveTime <= 0)
            {
                DestroyObject(this.gameObject);
            }
            ActiveTime--;
        }
        moveable.CheckY();
    }
    private bool Grounded()
    {
        float HalfWidth = (transform.lossyScale.x * box.size.x) / 2.0f;
        return GroundRaycastCheck(0) || GroundRaycastCheck(HalfWidth) || GroundRaycastCheck(-HalfWidth);
    }
    private bool GroundRaycastCheck(float offset)
    {
        float HalfHeight = (transform.lossyScale.y * box.size.y) / 2.0f;
        RaycastHit2D[] hit = Physics2D.RaycastAll(new Vector2(transform.position.x + offset, transform.position.y - HalfHeight + .05f), Vector2.down, .1f);
        int size = hit.Length;
        for (int i = 0; i < size; i++)
        {
            if (hit[i].transform.tag.Equals("Ground"))
            {
                return true;
            }
        }
        return false;
    }
    public void start()
    {
        bomb = GetComponent<SpriteRenderer>();
        box = GetComponent<BoxCollider2D>();
        fire = transform.GetChild(0).GetComponent<ParticleSystem>();
        body = GetComponent<Rigidbody2D>();
        moveable = new Moveable(body, maxDownVelocity);
        hurtBox = transform.GetChild(1).GetComponent<HurtBox>();
        childBox = transform.GetChild(2).GetComponent<Collider2D>();
        hurtBox.setParent(this);
        active = false;
        childBox.enabled = false;
        fire.Stop();
    }
    public void move(float x, float y)
    {
        moveable.move(x,y);
    }

    public void gotHit(Collision2D hit)
    {
        if (!active && hit.gameObject.tag.Equals("Player"))
        {
            HitBox gotHitBox = hit.gameObject.GetComponent<HitBox>();
            int direction = 1;
            if (hit.gameObject.transform.position.x - transform.position.x < 0)
                direction = -1;
            moveable.moveY(gotHitBox.knockbackY * factor);
            moveable.setForceX(gotHitBox.knockbackX * direction *factor);
        }
    }
    public void HitConnected(Collision2D hit) { }
}
