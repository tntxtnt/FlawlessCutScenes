﻿using UnityEngine;
using System;
using System.Text;
using System.IO;

public class SaveBlock : MonoBehaviour
{
    public string LevelName;
    public string SongName;

    private Animator animator;
    private bool active;

    void Start()
    {
        animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (Time.timeScale == 0) return;
        if (active && Input.GetButtonDown("Up"))
            Run();
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            active = true;
            if (Input.GetButtonDown("Up"))
                Run();
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            active = false;
    }

    private void Run()
    {
        DateTime localDate = DateTime.Now;
        animator.SetBool("Press", true);
        string[] temp = { "Game Saved\n" + localDate.ToString()};
        int[] tempSize = { 30, 30 };
        GameManager.instance.sendTextBoxMessage(temp,tempSize);
		GameManager.instance.Save (LevelName,SongName);
    }

}
