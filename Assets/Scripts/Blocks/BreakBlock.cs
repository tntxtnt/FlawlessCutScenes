﻿using UnityEngine;
using System.Collections;

public class BreakBlock : MonoBehaviour, HitAble
{
    public int Hp;
    void Start ()
    {
        transform.GetChild(0).GetComponent<HurtBox>().setParent(this);
    }

    public void gotHit(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            if (Hp <= hit.gameObject.GetComponent<HitBox>().getDamage())
                DestroyObject(this.gameObject);
        }
    }

    public void HitConnected(Collision2D hit) { }
}
