﻿using UnityEngine;
using System.Collections;

public class IndexPot : MonoBehaviour, HitAble
{

    public int index;
    public float offsetX;
    public float offsetY;
    public GameObject Item;
    private HurtBox hurtBox;

    void Start()
    {
        if (index != -1 && GameManager.instance.GottenItem[index])
        {
            Instantiate(Item, new Vector3(transform.position.x + offsetX, transform.position.y + offsetY, 0), Quaternion.identity);
            DestroyObject(this.gameObject);
        }
        hurtBox = transform.GetChild(0).GetComponent<HurtBox>();
        hurtBox.setParent(this);
    }

    public void gotHit(Collision2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            if (index != -1)
                GameManager.instance.GottenItem[index] = true;
            Instantiate(Item, new Vector3(transform.position.x+offsetX, transform.position.y+offsetY, 0), Quaternion.identity);
            DestroyObject(this.gameObject);
        }
    }

    void HitAble.HitConnected(Collision2D hit) { }
}
