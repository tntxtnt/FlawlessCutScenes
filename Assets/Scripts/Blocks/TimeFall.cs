﻿using UnityEngine;
using System.Collections;

public class TimeFall : MonoBehaviour 
{
    public int Timer;
    public int stop;
    public int end;
    private int counter;
    private Rigidbody2D body;
    private int state;
    void Start()
    {
        counter = 0;
        state = 0;
        body = gameObject.GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        if (state==0 && counter == Timer)
        {
            body.isKinematic = false;
            counter = 0;
            state = 1;
        }
        else if (state == 1 && counter == stop)
        {
            body.isKinematic = true;
            counter = 0;
            state = 2;
        }
        else if (state == 2 && counter == end)
            DestroyObject(this.gameObject);
        counter++;
    }
}
