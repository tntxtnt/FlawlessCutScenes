﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour
{
    public float newPointX;
    public float newPointY;
    public float newCameraX;
    public float newCameraY;

    private bool active;

    void Update()
    {
        if (Time.timeScale == 0) return;
        if (active && Input.GetButtonDown("Up"))
            run();
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            active = true;
            if (Input.GetButtonDown("Up"))
                run();
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            active = false;
    }

    private void run()
    {
        GameObject player = GameObject.Find("Player");
        GameObject camera = GameObject.Find("Main Camera");
        player.transform.position = new Vector3(newPointX, newPointY, player.transform.position.z);
        camera.transform.position = new Vector3(newCameraX, newCameraY, camera.transform.position.z);
    }
}
