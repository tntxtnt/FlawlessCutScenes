﻿using UnityEngine;
using System.Collections;

public class LockedBossDoor : MonoBehaviour {

    public int index;
    public GameObject Door;
    public float newPointX;
    public float newPointY;
    public float newCameraX;
    public float newCameraY;
    public string LevelName;
    public string SongName;

    private bool active;

    void Start()
    {
        if (GameManager.instance.GottenItem[index])
            OpenDoor();
    }
    void Update()
    {
        if (Time.timeScale == 0) return;
        if (active && GameManager.instance.BossKey && Input.GetButtonDown("Up"))
        {
            GameManager.instance.useBossKey();
            OpenDoor();
        }
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            active = true;
            if (GameManager.instance.BossKey && Input.GetButtonDown("Up"))
            {
                GameManager.instance.useBossKey();
                OpenDoor();
            }
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            active = false;
    }

    private void OpenDoor()
    {
        GameManager.instance.GottenItem[index] = true;
        DestroyObject(this.gameObject);
        GameObject newDoor = Instantiate(Door, transform.position, Quaternion.identity) as GameObject;
        Door door = newDoor.GetComponent<Door>();
        door.newPointX = newPointX;
        door.newPointY = newPointY;
        door.newCameraX = newCameraX;
        door.newCameraY = newCameraY;
        door.LevelName = LevelName;
        door.SongName = SongName;
        
    }
}
