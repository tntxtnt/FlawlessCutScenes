﻿using UnityEngine;
using System.Collections;

public class DoubleJumpChest : MonoBehaviour
{
    public int index;
    private Animator animator;
    private bool active;
    void Start()
    {
        animator = GetComponent<Animator>();
        if (GameManager.instance.GottenItem[index])
            animator.SetBool("Open", true);
    }
    void Update()
    {
        if (Time.timeScale == 0) return;
        if (active && Input.GetButtonDown("Up") && !animator.GetBool("Open") && GameManager.instance.Keys != 0)
        {
            GameManager.instance.updateKeys(-1);
            Open();
        }
    }
    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            active = true;
            if (Input.GetButtonDown("Up") && !animator.GetBool("Open") && GameManager.instance.Keys != 0)
            {
                GameManager.instance.updateKeys(-1);
                Open();
            }
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            active = false;
    }

    private void Open()
    {
        GameManager.instance.GottenItem[index] = true;
        animator.SetBool("Open", true);
        GameManager.instance.DoubleJump = true;
        string[] temp = { "Player Obtained Double Jump", "Press jump in the air to jump again" };
        GameManager.instance.sendTextBoxMessage(temp);
    }
}
