﻿using UnityEngine;
using System.Collections;

public class OneTimeText : MonoBehaviour
{
    public int index;
    [Multiline]
    public string[] message;

    public int[] fontSize;

    void Start()
    {
        if (GameManager.instance.GottenItem[index])
            DestroyObject(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            Run();
    }

    void OnTriggerStay2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            Run();
    }

    private void Run()
    {
        GameManager.instance.GottenItem[index] = true;
        DestroyObject(this.gameObject);
        if (fontSize.Length != message.Length)
            GameManager.instance.sendTextBoxMessage(message);
        else
            GameManager.instance.sendTextBoxMessage(message, fontSize);
    }
}
