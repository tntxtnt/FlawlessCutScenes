﻿using UnityEngine;
using System.Collections;

public class GroundBox : MonoBehaviour
{
    private bool ground;
    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.CompareTag("Ground"))
            ground = true;
    }

    void OnTriggerStay2D(Collider2D hit)
    {
        if (hit.CompareTag("Ground"))
            ground = true;
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.CompareTag("Ground"))
            ground = false;
    }
    public bool isGround()
    {
        return ground;
    }
    public void off()
    {
        ground = false;
    }
}
